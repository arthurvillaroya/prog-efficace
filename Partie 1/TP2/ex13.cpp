#include "eval-perf.h"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <immintrin.h>
#include <algorithm>

double getValue(int i, int j, int nbColonnes, std::vector<double> M){
    return M[i * nbColonnes + j];
}

void produitIJK(std::vector<double> A , std::vector<double> B, std::vector<double> & C, int nbCL, int nbLigneA, int nbColonB){
   
   for(int i = 0; i < nbLigneA; i++){
        for(int j = 0; j < nbColonB; j++){
            double res = 0;
            for(int k = 0; k < nbCL; k++){
                res += getValue(i,k, nbCL, A) * getValue(k,j, nbColonB,B);
            }
            C[i*nbColonB + j] = res;
        }
    }
}

void produitJIK(std::vector<double> A , std::vector<double> B, std::vector<double> & C, int nbCL, int nbLigneA, int nbColonB){

    for(int j = 0; j < nbColonB; j++){
        for(int i = 0; i < nbLigneA; i++){
            double res = 0;
            for(int k = 0; k < nbCL; k++){
                res += getValue(i,k, nbCL, A) * getValue(k,j, nbColonB,B);
            }
            C[i*nbColonB + j] = res;
        }
    }
}

void produitKIJ(std::vector<double> A , std::vector<double> B, std::vector<double> & C, int nbCL, int nbLigneA, int nbColonB){

    for(int k = 0; k < nbCL; k++){
        for(int i = 0; i < nbLigneA; i++){
            for(int j = 0; j < nbColonB; j++){
                C[i*nbColonB + j] += getValue(i,k, nbCL, A) * getValue(k,j, nbColonB,B);
            }
        }
    }
}

void produitKJI(std::vector<double> A , std::vector<double> B, std::vector<double> & C, int nbCL, int nbLigneA, int nbColonB){

    for(int k = 0; k < nbCL; k++){
        for(int j = 0; j < nbColonB; j++){
            for(int i = 0; i < nbLigneA; i++){
                C[i*nbColonB + j] += getValue(i,k, nbCL, A) * getValue(k,j, nbColonB,B);
            }
        }
    }
}

void produitJKI(std::vector<double> A , std::vector<double> B, std::vector<double> & C, int nbCL, int nbLigneA, int nbColonB){

    for(int j = 0; j < nbColonB; j++){
        for(int k = 0; k < nbCL; k++){
            for(int i = 0; i < nbLigneA; i++){
                C[i*nbColonB + j] += getValue(i,k, nbCL, A) * getValue(k,j, nbColonB,B);
            }
        }
    }
}

void produitIKJ(std::vector<double> A , std::vector<double> B, std::vector<double> & C, int nbCL, int nbLigneA, int nbColonB){

    for(int i = 0; i < nbLigneA; i++){
        for(int k = 0; k < nbCL; k++){
            for(int j = 0; j < nbColonB; j++){
                C[i*nbColonB + j] += getValue(i,k, nbCL, A) * getValue(k,j, nbColonB,B);
            }
        }
    }
}

void SIMD(std::vector<double> A , std::vector<double> B, std::vector<double> & C, int nbCL, int nbLigneA, int nbColonB){

    double buff[4];

    __m256d ireg; 
    __m256d jreg; 
    __m256d kreg;

    for(int i = 0; i < nbLigneA-4; i+=4){
        for(int k = 0; k < nbCL-4; k+=4){
            for(int j = 0; j < nbColonB-4; j+=4){
                kreg = _mm256_set_pd(getValue(i,j, nbColonB,C),getValue(i+1,j+1, nbColonB,C),getValue(i+2,j+2, nbColonB,C),getValue(i+3,j+3, nbColonB,C));

                ireg = _mm256_set_pd(getValue(i,k, nbCL, A),getValue(i+1,k+1, nbCL, A),getValue(i+2,k+2, nbCL, A),getValue(i+3,k+3, nbCL, A));

                jreg = _mm256_set_pd(getValue(k,j, nbColonB,B),getValue(k+1,j+1, nbColonB,B),getValue(k+2,j+2, nbColonB,B),getValue(k+3,j+3, nbColonB,B));

                kreg =  _mm256_fmadd_pd(ireg,jreg,kreg);

                _mm256_storeu_pd(buff,kreg);

                C[i*nbColonB + j] = buff[0];
                C[i+1*nbColonB + j+1] = buff[1];
                C[i+2*nbColonB + j+2] = buff[2];
                C[i+3*nbColonB + j+3] = buff[3];

            }
        }
    }
    for(int i = 0; i < nbLigneA%4; i++){
        for(int k = 0; k < nbCL%4; k++){
            for(int j = 0; j < nbColonB%4; j++){
                C[(i+(nbLigneA-nbLigneA%4))*nbColonB + (j+(nbColonB-nbColonB%4))] += getValue((i+(nbLigneA-nbLigneA%4)),(k+(nbCL-nbCL%4)), nbCL, A) * getValue((k+(nbCL-nbCL%4)),(j+(nbColonB-nbColonB%4)), nbColonB,B);
            }
        }
    }
}

double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int main(){
    std::vector<double> A;
    std::vector<double> B;
    std::vector<double> C;

    for(unsigned long int i = 0; i < 50*50; i++){
        A.push_back(fRand(0,15));
        B.push_back(fRand(0,15));
    }

    int nbCL = 50;

    int nbLigneA = A.size()/nbCL;
    int nbColonB = B.size()/nbCL;
    C.resize(nbLigneA*nbColonB);
    unsigned long int taille = C.size();
    for(unsigned long int i = 0; i < taille; i++){C[i]=0;}

    int N = nbCL*nbColonB*nbLigneA*1000;

    EvalPerf PE;
    PE.start();
    produitIJK(A,B,C,nbCL,nbLigneA,nbColonB);
    PE.stop();
    std::cout<<"////////////////////produitIJK///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)<<std::endl;

    PE.start();
    produitJIK(A,B,C,nbCL,nbLigneA,nbColonB);
    PE.stop();
    std::cout<<"////////////////////produitJIK///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)<<std::endl;

    PE.start();
    produitKIJ(A,B,C,nbCL,nbLigneA,nbColonB);
    PE.stop();
    std::cout<<"////////////////////produitKIJ///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)<<std::endl; 

    PE.start();
    produitKJI(A,B,C,nbCL,nbLigneA,nbColonB);
    PE.stop();
    std::cout<<"////////////////////produitKJI///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)<<std::endl; 

    PE.start();
    produitJKI(A,B,C,nbCL,nbLigneA,nbColonB);
    PE.stop();
    std::cout<<"////////////////////produitJKI///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)<<std::endl; 

    PE.start();
    produitIKJ(A,B,C,nbCL,nbLigneA,nbColonB);
    PE.stop();
    std::cout<<"////////////////////produitIKJ///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)<<std::endl;

    PE.start();
    produitIKJ(A,B,C,nbCL,nbLigneA,nbColonB);
    PE.stop();
    std::cout<<"////////////////////SIMD///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)<<std::endl;      


    return 0;
}