#include "eval-perf.h"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <immintrin.h>


template <typename T>
void reduceP(const std::vector<T> &V, T &res){
	res = 1;
	size_t taille = V.size();
	for(size_t i = 0; i < taille; i++){
		res *= V [i];
	}
}
template <typename T>
void reduceS(const std::vector<T> &V, T &res){
	res = 0;
	size_t taille = V.size();
	for(size_t i = 0; i < taille; i++){
		res += V [i];
	}
}

void reduce1(const std::vector<double> &V, double &res){
	res = 1;
	__m256d buff =_mm256_loadu_pd(&(V[0]));
	size_t taille = V.size();
	size_t i;
	for(i = 4; i < taille-4; i += 4){
		__m256d buff2 =  _mm256_loadu_pd(&V[0]+i);
		buff = _mm256_mul_pd(buff, buff2);
	}

	std::vector<double> New(4);
	_mm256_storeu_pd(&New[0], buff);


	for(int i = 0; i < 4; i++){
		res *= New[i];
	}
	for(i; i < taille; i++){
		res *= V[i];
	}
}

void reduce2(const std::vector<int32_t> &V, int &res){
	res = 0;
	__m256i buff = _mm256_loadu_si256((__m256i*) &(V[0]));
	size_t taille = V.size();
	size_t i;
	for(i = 8; i < taille-8; i += 8){
		__m256i buff2 =  _mm256_loadu_si256((__m256i*) (&V[0]+i));
		buff = _mm256_add_epi32(buff, buff2);
	}
	
	std::vector<int32_t> New(8);
	_mm256_storeu_si256((__m256i*)&New[0], buff);


	for(int i = 0; i < 8; i++){
		res += New[i];
	}
	for(i; i < taille; i++){
		res += V[i];
	}

}

int main(){
	std::vector<double> polydouble(1000000);
    unsigned long int taille = polydouble.size();
    for(long unsigned int i = 0; i < taille; i++){
        polydouble[i] = 3.56;
    }

    unsigned long int N;
    N = taille;

    EvalPerf PE;
    PE.start();
    double dou;
	reduceP(polydouble, dou);
	PE.stop();
	std::cout<<"////////////////////PRODUIT///////////////// : "<<std::endl;
    std::cout<<"resultat    "<<dou<<std::endl; 
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;

	PE.start();
	reduce1(polydouble, dou);
	PE.stop();
 
	std::cout<<"////////////////////mmd256///////////////// : "<<std::endl;
	std::cout<<"resultat    "<<dou<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;

	std::vector<int> polyint(1000000);
    taille = polyint.size();
    for(long unsigned int i = 0; i < taille; i++){
        polyint[i] = 3;
    }

	int32_t entier;
	PE.start();
	reduceS(polyint, entier);
	PE.stop();
	std::cout<<"////////////////////SOMME///////////////// : "<<std::endl;
	std::cout<<"resultat    "<<entier<<std::endl; 
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;

	PE.start();
	reduce2(polyint, entier);
	PE.stop();
	std::cout<<"////////////////////int32_t///////////////// : "<<std::endl;
	std::cout<<"resultat    "<<entier<<std::endl; 
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;



	return 0;
}