#include "eval-perf.h"
#include <iostream>
#include <stdlib.h>
#include <vector>

template<typename T>
T polynome(std::vector<T> &poly, T alpha){
    T res = 0;
    T acc = alpha;
    unsigned long int taille = poly.size();
    for(long unsigned int i = 0; i < taille; i++){
        res += acc*poly[i];
        acc *= alpha;
    }

    return res;
}

template<typename T>
T polynomeOpti(std::vector<T> &poly, T alpha){
    T res = 0;
    T acc = alpha;
    unsigned long int taille = poly.size();
    for(long unsigned int i = 0; i < taille+6; i = i + 6){
        res += acc*poly[i];
        acc *= alpha;
        res += acc*poly[i+1];
        acc *= alpha;
        res += acc*poly[i+2];
        acc *= alpha;
        res += acc*poly[i+3];
        acc *= alpha;
        res += acc*poly[i+4];
        acc *= alpha;
        res += acc*poly[i+5];
        acc *= alpha;
    }

    return res;
}

int main(){
    std::vector<int> polyint(100000000);
    unsigned long int taille = polyint.size();
    for(long unsigned int i = 0; i < taille; i++){
        polyint[i] = 1;
    }

    unsigned long int N;
    N = taille;

    EvalPerf PE;
    PE.start();
    int entier = 5;
	polynome(polyint, entier);
	PE.stop();
    std::cout<<"////////////////////Polynome///////////////// : "<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;

	PE.start();
	polynomeOpti(polyint, entier);
	PE.stop();
	std::cout<<"////////////////////polynomeOpti///////////////// : "<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;
    return 0;
}