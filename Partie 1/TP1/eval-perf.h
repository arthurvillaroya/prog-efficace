#include <iostream>
#include <chrono>
#include <x86intrin.h>

class EvalPerf{

	private:
		std::chrono::time_point<std::chrono::high_resolution_clock> begin;
		std::chrono::time_point<std::chrono::high_resolution_clock> end;

		uint64_t beginCycle;
		uint64_t endCycle;

		uint64_t rdtsc(){
			unsigned int lo, hi;
			__asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
			return ((uint64_t)hi << 32) | lo;
		}

	public:

		void start(){
			begin = std::chrono::high_resolution_clock::now();
			beginCycle = rdtsc();
		}

		void stop(){
			end = std::chrono::high_resolution_clock::now();
			endCycle = rdtsc();
		}

		uint64_t nb_cycle(){
			return endCycle - beginCycle;
		}

		double second(){
			return 	std::chrono::duration_cast<std::chrono::seconds>(end-begin).count();
		}

		double millisecondes(){
			return std::chrono::duration_cast<std::chrono::milliseconds>(end-begin).count();
		}

		double IPC(uint64_t N){
			return double(N)/nb_cycle();   
		}

		double CPI(uint64_t N){
			return nb_cycle()/double(N);
		}
}; 
