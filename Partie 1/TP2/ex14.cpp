#include "eval-perf.h"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <immintrin.h>
#include <algorithm>
#include <math.h>


int64_t getValue(int i, int j, int nbColonnes, std::vector<int64_t> M){
    return M[i * nbColonnes + j];
}

void transpose(std::vector<int64_t> M, std::vector<int64_t> &Mt){
	int nbCol = sqrt(M.size());
	for(int i = 0; i < nbCol; i++){
		for(int j = 0; j < nbCol; j++){
			Mt[i*nbCol+j] = getValue(j,i,nbCol,M);
		}		
	}
}

int main(){
	std::vector<int64_t> M;
	std::vector<int64_t> Mt;

	M.push_back(14);M.push_back(10);M.push_back(3);M.push_back(15);
	M.push_back(8);M.push_back(4);M.push_back(4);M.push_back(6);
	M.push_back(4);M.push_back(10);M.push_back(16);M.push_back(12);
	M.push_back(15);M.push_back(1);M.push_back(1);M.push_back(10);

	Mt.resize(M.size());
	transpose(M,Mt);

	for(int i = 0; i < Mt.size(); i++){
		std::cout<<Mt[i]<<std::endl;
	}


	return 0;
}