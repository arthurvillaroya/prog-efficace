#include "eval-perf.h"
#include <iostream>
void fonction(int N){
	int a = 3;
	int b = 4;
	int y;
	for(int i = 0; i<N; i++){
		y = (a + b);
		y++;
	}
}

int main(){
	uint64_t n, N;
	EvalPerf PE;
	n = 10000000000;
	N = 2*n;
	PE.start();
	fonction(n);
	PE.stop();
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;

	return 0;
}