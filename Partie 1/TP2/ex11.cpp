#include "eval-perf.h"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <immintrin.h>
#include <algorithm>



void naif(const std::vector<float> &V, float &res){
	res = std::numeric_limits<float>::max();
	size_t taille = V.size();
	size_t i;
	for(i = 0; i < taille; i++){
		if(res > V[i]){res = V[i];}
	}
}

void SIMD(const std::vector<float> &V, float &res){
	res = std::numeric_limits<float>::max();
	__m256 buff =_mm256_loadu_ps(&(V[0]));
	size_t taille = V.size();
	size_t i;
	for(i = 8; i < taille-8; i += 8){
		__m256 buff2 =  _mm256_loadu_ps(&V[0]+i);
		buff = _mm256_min_ps(buff, buff2);
	}

	std::vector<float> New(8);
	_mm256_storeu_ps(&New[0], buff);


	for(int i = 0; i < 8; i++){
		if(res > New[i]){res = New[i];}
	}
}


int main(){
	std::vector<float> polydouble(1000000);
    unsigned long int taille = polydouble.size();
    polydouble[0] = 1;
    for(long unsigned int i = 1; i < taille; i++){
        polydouble[i] = 3.56;
    }

    unsigned long int N;
    N = taille;

    EvalPerf PE;
    PE.start();
    float dou;
	naif(polydouble, dou);
	PE.stop();
	std::cout<<"////////////////////Naif///////////////// : "<<std::endl;
    std::cout<<"resultat    "<<dou<<std::endl; 
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;

	PE.start();
	SIMD(polydouble, dou);
	PE.stop();
 
	std::cout<<"////////////////////SIMD///////////////// : "<<std::endl;
	std::cout<<"resultat    "<<dou<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;


	PE.start();
	float t = *std::min_element(&polydouble[0], &polydouble[polydouble.size() - 1]);
	PE.stop();
	std::cout<<"////////////////////STD///////////////// : "<<std::endl;
	std::cout<<"resultat    "<<t<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;

	return 0;
}