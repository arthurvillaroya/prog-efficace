#include "eval-perf.h"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <immintrin.h>
#include <algorithm>

void sommeVectorCPU(const std::vector<float> &A, const std::vector<float> &B, std::vector<float> &C){
  for(int i = 0; i < C.size(); i++){
    C[i] = A[i]+ B[i];
  }
}

__global__ void sommeVectorGPU(const std::vector<float> &A, const std::vector<float> &B, std::vector<float> &C, unsigned int n){
  
  int size = n * sizeof(float); std::vector<float> d_A, d_B, d_C;
  
  cudaMalloc((void **) &d_A, size);
  cudaMalloc((void **) &d_B, size);
  cudaMalloc((void **) &d_C, size);

  cudaMemcpy(d_A, A, size, cudaMemcpyHostToDevice);
  cudaMemcpy(d_B, B, size, cudaMemcpyHostToDevice);

  for(int i = 0; i < sizeof(d_C); i++){
    d_C[i] = d_A[i]+ d_B[i];
  }

  cudaMemcpy(h_C, d_C, size, cudaMemcpyDeviceToHost);
  cudaFree(d_A); cudaFree(d_B); cudaFree (d_C);
}

void creatVector(std::vector<float> &C){
  C.resize(10);
  for(int i = 0; i < C.size(); i++){
    C[i] = i;
  }
}

int main(){
  std::vector<float> A;
  std::vector<float> B;
  std::vector<float> C;
  unsigned int n = C.size();
  creatVector(A);creatVector(B);
  sommeVectorCPU(A,B,C);
  sommeVectorGPU<<<1,1>>>(A,B,C,n);
  std::cout<<"Hello World!\n";
  return 0;
}
