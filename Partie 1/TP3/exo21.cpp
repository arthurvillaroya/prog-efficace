#include "eval-perf.h"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <immintrin.h>
#include <algorithm>


void MatrixTrans_naif(size_t n, const std::vector<double> &A, std::vector<double> &B){
    for(size_t i = 0; i < n; i++){
        for(size_t j = 0; j < n; j++){
            B[i + j*n] = A[j + i*n];
        }
    }
}

void trasnblocMini(int min, int max, size_t n, const std::vector<double> &A, std::vector<double> &B){
	for(size_t i = min; i < max && i < n; i++){
        for(size_t j = min; j < max && j < n; j++){
            B[i + j*n] = A[j + i*n];
        }
    }
}

void transBloc(size_t n, const std::vector<double> &A, std::vector<double> &B){
    for(size_t i = 0; i < n; i += 8){
        for(size_t j = 0; j < n; j += 8){
            trasnblocMini(i, i+8, n, A, B);
        }
    }
}

double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}


int main(){
	std::vector<double> A;
    std::vector<double> B;
    size_t n = 8;

    A.resize(n*n);
    B.resize(n*n);

    for(unsigned long int i = 0; i < n*n; i++){
        A.push_back(fRand(0,15));
    }
    

    EvalPerf PE;

    int N = n*n;
    PE.start();
    for(int i = 0; i < 10000000; i++){
        MatrixTrans_naif(n, A, B);
    }
    PE.stop();
    std::cout<<"////////////////////transposeNaif///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()/10000000<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()/10000000<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()/10000000<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)/10000000<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)/10000000<<std::endl;

    PE.start();
    for(int i = 0; i < 10000000; i++){
        transBloc(n, A, B);
    }
    PE.stop();
    std::cout<<"////////////////////transposeNaif///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()/10000000<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()/10000000<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()/10000000<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)/10000000<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)/10000000<<std::endl;

	return 0;
}