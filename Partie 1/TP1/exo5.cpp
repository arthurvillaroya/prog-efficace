#include "eval-perf.h"
#include <iostream>
#include <stdlib.h>
#include <vector>

void sommePrefixe(std::vector<int> &tab, uint64_t n){
    unsigned int taille = tab.size();
    for(uint64_t i = 0; i < n; i++){
        for(unsigned long int i = 0; i < taille; i++){
            tab[i] += tab[i-1];
        }
    }
}

void afficher(std::vector<int> tab){
    unsigned int taille = tab.size();
    for(unsigned long int i = 0; i < taille; i++){
        std::cout<<"<"<<tab[i]<<"> ";
    }
}

int main(){
    uint64_t n = 1000000;
    long a = 500; // ici a est uniquement là pour augmenter le nombre d'itération pour avoir des résultats utiles. 
	std::vector<int>  tab(n);
    unsigned long int N;
	EvalPerf PE;
	N = tab.size()*a;
	PE.start();
	sommePrefixe(tab,a);
	PE.stop();

	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;

	return 0;
}