#include "eval-perf.h"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include "math.h"

#define C1 0.2f
#define C2 0.3f

void slowperformance1(std::vector<float> x, std::vector<float> y, std::vector<float> z, int n){
	for(int i = 0; i < n - 2; i++){
		x[i] = x[i]/M_SQRT2 + y[i] * C1;
		x[i+1] += z[(i%4) * 10] * C2;
		x[i+2] += sin((2* M_PI*i)/3) * y[i+2];
	}
}

void slowperformance2(std::vector<float> x, std::vector<float> y, std::vector<float> z, int n){
	x[0] = x[0]/M_SQRT2 + y[0] * C1;
	x[1] = x[1]+[(0%4) * 10] * C2;
	for(int i = 0; i < n - 2; i++){	
		x[i] = (sin((2* M_PI*i)/3) * y[i] + z[(i%4) * 10] * C2 + (x[i]/M_SQRT2 + y[i] * C1) + x[i])/M_SQRT2 + y[i] * C1;
	}
	x[n-2] = sin((2* M_PI*n-2)/3) * y[n-2] + z[(n-2%4) * 10] * C2;
}

int main (){

	std::vector<float> X(10000);
	std::vector<float> Y(10000);
	std::vector<float> Z(10000);

	unsigned long int taille = X.size();
	for(unsigned long int i = 0; i < taille; i++){
		X[i] = rand() % 100 + 1;
		Z[i] = rand() % 100 + 1;
		Y[i] = rand() % 100 + 1;
	}


	EvalPerf PE;
    PE.start();
    int entier;
	slowperformance1(X, Y, Z, X.size());
	PE.stop();
	unsigned long int N;
    N = taille*10;
    std::cout<<"////////////////////slowperformance1///////////////// : "<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;
	return 0;
}