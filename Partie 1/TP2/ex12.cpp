#include "eval-perf.h"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <immintrin.h>
#include <algorithm>
#include <math.h>


double polynome(std::vector<float> &poly, int alpha){
    double res = poly[0];
    int acc = alpha;
    unsigned long int taille = poly.size();
    for(long unsigned int i = 1; i < taille; i++){
        res += acc*poly[i];
        acc *= alpha;
    }

    return res;
}

double polySIMD(std::vector<float> &poly, int x)
{
	double res = 0; double acc = x; unsigned long int degre = poly.size()-1;
	 
	__m256d p_vec,x_vec, y_vec,f_vec;

	f_vec = _mm256_set_pd(1,x,x*x,x*x*x);

	p_vec = _mm256_set_pd(poly[0],poly[1],poly[2],poly[3]);

	x_vec = _mm256_set_pd(x*x*x*x,x*x*x*x,x*x*x*x,x*x*x*x);

	int i = 4;
	for(; i <= degre - 3; i+=4)
	{
		y_vec = _mm256_set_pd(poly[i],poly[i+1],poly[i+2],poly[i+3]);

		p_vec = _mm256_fmadd_pd(x_vec,y_vec,p_vec);

		for(int y = 0; y < 4; y++)
		{
			x_vec = _mm256_mul_pd(x_vec,x_vec);
		}
	}

	p_vec = _mm256_mul_pd(p_vec,f_vec);

	double temp[4];
	_mm256_storeu_pd(temp,p_vec);

	res += temp[0] + temp[1] + temp[2] + temp[3];

	if( degre % 4 != 0)
	{
		double acc = pow(x,degre - (degre%4));

		for(int i = 0 ;i < degre;i++)
		{
			res += poly[i]  * acc;
			acc *= x;
		}
	}

	return res;
}




int main(){
    std::vector<float> polyint(100000000);
    unsigned long int taille = polyint.size();
    for(long unsigned int i = 0; i < taille; i++){
        polyint[i] = 3.6;
    }

    unsigned long int N;
    N = taille * 3;

    EvalPerf PE;
    PE.start();
    int entier = 5;
	double resultat = polynome(polyint, entier);
	PE.stop();
	std::cout<<"////////////////////POLY CLASSIQUE///////////////// : "<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl; 
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;

	N = taille*3;
	PE.start();
	resultat = polySIMD(polyint, entier);
	PE.stop();
	std::cout<<"////////////////////POLY SIMD///////////////// : "<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl; 
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;



	return 0;
}