#include "eval-perf.h"
#include <iostream>
#include <stdlib.h>
#include <vector>


template<typename T>
T polynome(std::vector<T> &poly, T alpha){
    T res = poly[0];
    T acc = alpha;
    unsigned long int taille = poly.size();
    for(long unsigned int i = 1; i < taille; i++){
        res += acc*poly[i];
        acc *= alpha;
    }

    return res;
}

template<typename T>
T horner(std::vector<T> &poly, T alpha){
    T res = 0;
    unsigned long int taille = poly.size() - 1;
    for(long int i = taille; i >= 0; i--){
        res *= alpha;
        res += poly[i]; 
    }

    return res;
}

int main(){
    std::vector<int> polyint(100000000);
    unsigned long int taille = polyint.size();
    for(long unsigned int i = 0; i < taille; i++){
        polyint[i] = 1;
    }

    unsigned long int N;
    N = taille * 3;

    EvalPerf PE;
    PE.start();
    int entier = 5;
	int resultat = polynome(polyint, entier);
	PE.stop();
    std::cout<<"///////////////////INT///////////////// : "<<std::endl;
    std::cout<<"////////////////////POLY///////////////// : "<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;

    N = taille * 2 + 1;
    PE.start();
	resultat = horner(polyint, entier);
	PE.stop();
    std::cout<<"////////////////////Horner///////////////// : "<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;


    std::vector<double> polyfloat(100000000);
    taille = polyfloat.size();
    for(long unsigned int i = 0; i < taille; i++){
        polyfloat[i] = 1.0;
    }

    N = taille * 3;

    PE.start();
    double flot = 5.0;
	double res = polynome(polyfloat, flot);
	PE.stop();
    std::cout<<"///////////////////FLOAT///////////////// : "<<std::endl;
    std::cout<<"////////////////////POLY///////////////// : "<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;

    N = taille * 2 + 1;
    PE.start();
	res = horner(polyfloat, flot);
	PE.stop();
    std::cout<<"////////////////////Horner///////////////// : "<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;
    return 0;
}