#include "eval-perf.h"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <immintrin.h>
#include <algorithm>

void mat2vec(size_t n, const std::vector<double> A, std::vector<double> &y, std::vector<double> &tmp, size_t k, std::vector<double> v){
	for(int i = 0; i < n; ++i){
		for(int j = 0; j < n; j+=k){
			tmp[i] += A[i*n + j] * v[j];
		}
	}

	for(int i = 0; i < n; ++i){
		for(int j = 0; j < n; j+=k){
			y[i] += A[i*n + j] * tmp[j];
		}
	}
}

void matTranspose(size_t n, const std::vector<double> A, std::vector<double> &y, std::vector<double> &tmp, size_t k, std::vector<double> v){
	for(int i = 0; i < n; ++i){
		for(int j = 0; j < n; j+=k){
			tmp[i] += A[j*n + i] * v[j];
		}
	}


	for(int i = 0; i < n; ++i){
		for(int j = 0; j < n; j+=k){
			y[i] += A[i*n + j] * tmp[j];
		}
	}
}

double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int main(){
	std::vector<double> A;
    std::vector<double> y;
    std::vector<double> tmp;
    std::vector<double> v;
    int n = 50;

    A.resize(n*n);
    v.resize(n);

    for(unsigned long int i = 0; i < n*n; i++){
        A.push_back(fRand(0,15));
    }
    for(unsigned long int i = 0; i < n; i++){
        v.push_back(fRand(0,15));
    }
    
    int k;

    y.resize(n);
    tmp.resize(n);
    for(unsigned long int i = 0; i < n; i++){tmp[i]=0;}
    for(unsigned long int i = 0; i < n; i++){y[i]=0;}

    int N = 4*n*n;

    EvalPerf PE;

    k = 1;
    PE.start();
    mat2vec(n, A, y, tmp, k, v);
    PE.stop();
    std::cout<<"////////////////////K1mat2///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)<<std::endl;

    PE.start();
    matTranspose(n, A, y, tmp, k, v);
    PE.stop();
    std::cout<<"////////////////////K1matTranspose///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)<<std::endl;
	
	N = (4*n*n)/7;
	k = 7;
    PE.start();
    mat2vec(n, A, y, tmp, k, v);
    PE.stop();
    std::cout<<"////////////////////K7mat2///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)<<std::endl;

    PE.start();
    matTranspose(n, A, y, tmp, k, v);
    PE.stop();
    std::cout<<"////////////////////K7matTranspose///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)<<std::endl;

    N = (4*n*n)/16;
    k = 16;
    PE.start();
    mat2vec(n, A, y, tmp, k, v);
    PE.stop();
    std::cout<<"////////////////////K16mat2///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)<<std::endl;

    PE.start();
    matTranspose(n, A, y, tmp, k, v);
    PE.stop();
    std::cout<<"////////////////////K16matTranspose///////////////// : "<<std::endl;
    std::cout<<"nbr cycles  "           <<PE.nb_cycle()<<std::endl;
    std::cout<<"nbr secondes:  "        <<PE.second()<<std::endl;
    std::cout<<"nbr millisecondes: "    <<PE.millisecondes()<<std::endl;
    std::cout<<"CPI= "                  <<PE.CPI(N)<<std::endl; 
    std::cout<<"IPC= "                  <<PE.IPC(N)<<std::endl;

	return 0;
}