#include "eval-perf.h"
#include <iostream>
#include <stdlib.h>
#include <vector>


template <typename T>
void reduceP(const std::vector<T> &V, T &res){
	res = 0;
	size_t taille = V.size();
	for(size_t i = 0; i < taille; i++){
		res += V [i];
	}
}

template <typename T>
void reduceF(const std::vector<T> &V, T &res){
	res = 1;
	size_t taille = V.size();
	for(size_t i = 0; i < taille; i++){
		res *= V [i];
	}
} 

template <typename T>
void reducePD(const std::vector<T> &V, T &res){
	res = 0;
	size_t taille = V.size();
	for(size_t i = 0; i < taille-15; i = i + 15){
		res += V [i];
		res += V [i+1];
		res += V [i+2];
		res += V [i+3];
		res += V [i+4];
		res += V [i+5];
		res += V [i+6];
		res += V [i+7];
		res += V [i+8];
		res += V [i+9];
		res += V [i+10];
		res += V [i+11];
		res += V [i+12];
		res += V [i+13];
		res += V [i+14];
	}
}

template <typename T>
void reduceFD(const std::vector<T> &V, T &res){
	res = 1;
	size_t taille = V.size();
	for(size_t i = 0; i < taille-15; i = i + 15){
		res *= V [i];
		res *= V [i+1];
		res *= V [i+2];
		res *= V [i+3];
		res *= V [i+4];
		res *= V [i+5];
		res *= V [i+6];
		res *= V [i+7];
		res *= V [i+8];
		res *= V [i+9];
		res *= V [i+10];
		res *= V [i+11];
		res *= V [i+12];
		res *= V [i+13];
		res *= V [i+14];
	}
}  


int main(){
    std::vector<int> polyint(100000000);
    unsigned long int taille = polyint.size();
    for(long unsigned int i = 0; i < taille; i++){
        polyint[i] = 1;
    }

    unsigned long int N;
    N = taille;

    EvalPerf PE;
    PE.start();
    int entier;
	reduceP(polyint, entier);
	PE.stop();
    std::cout<<"////////////////////Plus///////////////// : "<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;

	PE.start();
	reduceF(polyint, entier);
	PE.stop();
	std::cout<<"////////////////////Fois///////////////// : "<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;

	PE.start();
	reducePD(polyint, entier);
	PE.stop();
	std::cout<<"///////////////////DEROULER///////////////// : "<<std::endl;
    std::cout<<"////////////////////PlusD///////////////// : "<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;

	PE.start();
	reduceFD(polyint, entier);
	PE.stop();
	std::cout<<"////////////////////FoisD///////////////// : "<<std::endl;
	std::cout<<"nbr cycles  "    		<<PE.nb_cycle()<<std::endl;
	std::cout<<"nbr secondes:  " 		<<PE.second()<<std::endl;
	std::cout<<"nbr millisecondes: "	<<PE.millisecondes()<<std::endl;
	std::cout<<"CPI= "					<<PE.CPI(N)<<std::endl;
	std::cout<<"IPC= " 					<<PE.IPC(N)<<std::endl;
    return 0;
}